#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main (int argc, char **argv)
{
  unsigned long int lines, columns;
  register unsigned long int i, j, k;
  long int *matA;
  long int *matB;
  long int *matResult;
  long int somatorio;
  float p;  

  // Verifica os parâmetros de entrada
  switch(argc)
  {
      case 0:
      case 1:
      case 2:
          fprintf(stderr, "Uso:\n\t%s <nº de linhas> <nº de colunas>\n", argv[0]);
          exit(EXIT_FAILURE);
          break;
      case 3:
          lines = atoi(argv[1]);
          columns = atoi(argv[2]);
  }

  // Alocação dinâmica das matrizes
  matA = (long int *) malloc(sizeof(long int) * lines * columns);
  matB = (long int *) malloc(sizeof(long int) * lines * columns);
  matResult = (long int *) malloc(sizeof(long int) * lines * columns);

  // Verifica se alocou todas as matrizes
  if ((matA == NULL)||(matB == NULL)||(matResult == NULL))
  {
      perror("Estourou a memória!\n");
      exit(EXIT_FAILURE);
      return -1;
  }

  // Gera matrizes A e B com números aleatórios
  srand( time(NULL) );
  for (i=0; i<lines; i++)
  {
      for(j=0; j<columns; j++)
      {
         matA[(i*columns) + j] = rand() % (lines*columns);
	 matB[(i*columns) + j] = rand() % (lines*columns);
      }
  }
  
  // Mutiplica A x B e guarda em MatResult.
  for (i=0; i<lines; i++)
  {
	for (j=0; j<columns; j++)
	{
		somatorio=0;
		for (k=0; k<columns; k++)
		{
			somatorio += matA[(i*columns) + k] * matB[(k*columns) + j];
		}
		matResult[(i*columns) + j] = somatorio;
	}
	p = (float)i / lines * 100;
     	printf("Calculo --> %.2f%%\n", p);	
  } 
	 
  // Desaloca as matrizes
  free(matA);
  free(matB);
  free(matResult);

  return 0;
}
