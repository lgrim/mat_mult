/***************************************************************************
 * FILE: ser_2dfft.c
 * OTHER FILES: ser_fft.c ser_fft.h
 * DESCRIPTION: 
 *   Serial - Two-Dimensional Fast Fourier Transform - C Version
 *   Algorithm: Perform a 1D FFT on each row, transpose, 
 *   then perform a 1D FFT on each row again.  
 *   Input is an complex matrix. Output is an complex matrix that overwrites 
 *   the input matrix.  The input matrix is initialized with a point source.
 *   A straightforward unsophisticated 1D FFT kernel is used.  It is
 *   sufficient to convey the general idea, but be aware that there are
 *   better 1D FFTs available on many systems.
 * AUTHOR: George Gusciora
 * LAST REVISED:  04/15/05 Blaise Barney
***************************************************************************/
#include "ser_fft.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#define IMAGE_SIZE              1024

static mycomplex a[MAXN][MAXN];     /* input matrix */
static mycomplex w_common[MAXN/2];  /* twiddle factors */

int main(int argc, char *argv[])
{
  int n;           /* FFT size */
  int logn;        /* log base 2 of n */
  int errors,sign; /* used for error checking */
  int nx;          /* used to compute logn */
  int flops;       /* total number of floating point ops */
  float mflops;    /* Mflops/s */
  double fsecs;    /* time spent doing 1D FFT's */
  double tsecs;    /* time spent doing the transpose */
  double secs;     /* total time of the 2D FFT */
  int i,j;         /* index variables */

  struct timeval start, finish;      /* gettimeofday structures */
  /* Forward routine definitions */
  void fft();
  void print_cmat(); 
  void print_cvec(); 

  n = IMAGE_SIZE;
  printf("Starting serial version of 2D FFT using matrix size [%d][%d]\n",n,n);
  /* Compute logn and ensure that n is a power of two */
  nx = n;
  logn = 0;
  while(( nx >>= 1) > 0) 
    logn++; 
  nx = 1;
  for (i=0; i<logn; i++)
    nx = nx*2;
  if (nx != n) {
    (void)fprintf(stderr, "%s: fft size must be a power of 2\n",argv[0]);
    exit(0);
  }
  
  /* Initialize the input matrix with a centered point source */
  printf("Initializing matrix...\n");
  for (i=0; i<n; i++)
    for (j=0; j<n; j++) 
      a[i][j].r = a[i][j].i = 0.0;
  a[n/2][n/2].r =  a[n/2][n/2].i = (float)n; 

  /* Precompute the complex constants (twiddle factors) for the 1D FFTs */
  for (i=0;i<n/2;i++) {
    w_common[i].r = (float) cos((double)((2.0*PI*i)/(float)n));
    w_common[i].i = (float) -sin((double)((2.0*PI*i)/(float)n));
  }
  
  /* Now do the actual 2D FFT */
  printf("Starting 2D FFT...\n");
  /* First do a set of row FFTs */
  (void)gettimeofday(&start, 0);
  for (i=0; i<n; i++)
    (void)fft(&a[i][0], w_common, n, logn);
  (void)gettimeofday(&finish, 0);
  fsecs = ((((finish.tv_sec - start.tv_sec) * 1000000.0) +
	    finish.tv_usec) - start.tv_usec) / 1000000.0;

  /* Then transpose the matrix */
  (void)gettimeofday(&start, 0);
  for (i=0; i<n; i++)
    for (j=i+1; j<n; j++) 
      SWAP(a[i][j], a[j][i]); 
  (void)gettimeofday(&finish, 0);
  tsecs = ((((finish.tv_sec - start.tv_sec) * 1000000.0) +
	    finish.tv_usec) - start.tv_usec) / 1000000.0;
      
  /* Then do another set of row FFTs */
  (void)gettimeofday(&start, 0);
  for (i=0; i<n; i++)
    (void)fft(&a[i][0], w_common, n, logn);
  (void)gettimeofday(&finish, 0);
  fsecs += ((((finish.tv_sec - start.tv_sec) * 1000000.0) +
	    finish.tv_usec) - start.tv_usec) / 1000000.0;

  /* Transpose the matrix back */
  (void)gettimeofday(&start, 0);
  for (i=0; i<n; i++)
    for (j=i+1; j<n; j++) 
      SWAP(a[i][j], a[j][i]); 
  (void)gettimeofday(&finish, 0);
  tsecs = ((((finish.tv_sec - start.tv_sec) * 1000000.0) +
	    finish.tv_usec) - start.tv_usec) / 1000000.0;
       
  /*  print_cmat(a, n, n); */

  /* Check the answers for an alternating sequence of (+-n,+-n) */
  printf("Checking for errors...");
  errors = 0;
  for (i=0; i<n; i++) {
    if (((i+1)/2)*2 == i) 
      sign = 1;
    else
      sign = -1;
    for (j=0; j<n; j++) {
      if (a[i][j].r > n*sign+EPSILON ||
	  a[i][j].r < n*sign-EPSILON ||
	  a[i][j].i > n*sign+EPSILON ||
	  a[i][j].i < n*sign-EPSILON) {
	printf("%d,%d\n",i,j);
	errors++;
      }
      sign *= -1;
    }
  }
  if (errors) { 
    printf("%d errors!. Quitting.\n", errors);
    exit(1);
    }
  else
   printf("No errors found.\n");

  /* Summarize the 2d FFT performance */
  printf("Performance statistics for %d x %d 2D FFT:\n",n,n);
  secs = fsecs + tsecs;
  flops = (n*n*logn)*10;
  mflops = ((float)flops/1000000.0);
  mflops = mflops/(float)secs;
  printf("   1D FFTs   : %10.6f secs (%2d%%)\n", fsecs, (int)(fsecs/secs*100));
  printf("   Transpose : %10.6f secs (%2d%%)\n", tsecs, (int)(tsecs/secs*100));
  printf("   Total     : %10.6f secs\n", secs);
  printf("   %10.6f Mflop/s\n\n", mflops);
  exit(0);
}


/***************************************************************************
 * fft -  n-point in-place decimation-in-time FFT of complex vector 
 * "data" using the n/2 complex twiddle factors in "w_common".
 **************************************************************************/
void fft(data,w_common,n,logn)
mycomplex *data,*w_common;
int n,logn;
{
  int incrvec, i0, i1, i2, nx;
  float f0, f1;
  void bit_reverse();

  /* bit-reverse the input vector */
  (void)bit_reverse(data,n);

  /* Do the first logn-1 stages of the fft */
  i2 = logn;
  for (incrvec=2;incrvec<n;incrvec<<=1) {
    i2--;
    for (i0 = 0; i0 < incrvec >> 1; i0++) {
      for (i1 = 0; i1 < n; i1 += incrvec) {
        f0 = data[i0+i1 + incrvec/2].r * w_common[i0<<i2].r - 
	  data[i0+i1 + incrvec/2].i * w_common[i0<<i2].i;
        f1 = data[i0+i1 + incrvec/2].r * w_common[i0<<i2].i + 
	  data[i0+i1 + incrvec/2].i * w_common[i0<<i2].r;
        data[i0+i1 + incrvec/2].r = data[i0+i1].r - f0;
        data[i0+i1 + incrvec/2].i = data[i0+i1].i - f1;
        data[i0+i1].r = data[i0+i1].r + f0;
        data[i0+i1].i = data[i0+i1].i + f1;
      }
    }
  }

  /* Do the last stage of the fft */
  for (i0 = 0; i0 < n/2; i0++) {
    f0 = data[i0 + n/2].r * w_common[i0].r - 
      data[i0 + n/2].i * w_common[i0].i;
    f1 = data[i0 + n/2].r * w_common[i0].i + 
      data[i0 + n/2].i * w_common[i0].r;
    data[i0 + n/2].r = data[i0].r - f0;
    data[i0 + n/2].i = data[i0].i - f1;
    data[i0].r = data[i0].r + f0;
    data[i0].i = data[i0].i + f1;
  }
}

/***************************************************************************
 * bit_reverse - simple (but somewhat inefficient) bit reverse 
 **************************************************************************/
void bit_reverse(a,n)
mycomplex *a;
int n;
{
  int i,j,k;

  j = 0;
  for (i=0; i<n-2; i++){
    if (i < j)
      SWAP(a[j],a[i]);
    k = n>>1;
    while (k <= j) {
      j -= k; 
      k >>= 1;
    }
    j += k;
  }
}


/****************************************************************************
 * Diagnostic routines to print complex matrices and vectors
 * Commented out in main program for actual users
 ***************************************************************************/

void print_cvec(a, n)
mycomplex *a;
int n;
{
  int i;

  for (i=0; i<n; i++) {
    if (i%4 == 0 && i)  
      printf("\n"); 
    printf("(%.3f,%.3f) ", a[i].r, a[i].i);   
  }
  printf("\n");
}

void print_cmat(a, m, n)
mycomplex a[][MAXN];
int m,n;
{
  int i,j;

  for (i=0; i<m; i++) {
    for (j=0; j<n; j++)
      printf("(%6.2f,%6.2f) ",a[i][j].r, a[i][j].i);
    printf("\n");
  }
}
